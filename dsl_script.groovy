job('ExampleDSLExtension') {
  scm {
    git {
      remote {
        url('https://MichaelSandritter@bitbucket.org/MichaelSandritter/k1.git')
      }
      relativeTargetDir('source')
    }
  }
  steps {
    ant {
      antInstallation('Ant 1.9.6')
      target('prepare')
      buildFile('build/commit.xml')
      props('workspace': '$WORKSPACE',
        'COMPOSER.FOLDER': 'source')
    }
  }
  publishers {
   buildDependencyAnalysis {
     jsonPath('source/composer.json')
     lockPath('source/composer.lock')
   }
 }
}