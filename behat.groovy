import congo.AutoTaggingBuilder

def slurper = new ConfigSlurper()
slurper.classLoader = this.class.classLoader
def config = slurper.parse(readFileFromWorkspace(new File('src/configs', 'jobs.groovy').path))


config.typo3Extensions.each { key, jobConfig ->
    def jobName = 'congstar_extension_' + jobConfig.extensionName

    job(jobName) {
        description('Dieser Job wird über JobDSL konfiguriert. Alle manuellen Änderungen werden überschrieben.')
        multiscm {
            description('')
            logRotator {
                numToKeep(7)
            }
            wrappers {
                timestamps()
            }
            git {
                remote {
                    url(jobConfig.repositoryUrl)
                }
                relativeTargetDir('source')
            }
            git {
                remote {
                    url('git@git.aoesupport.com:libraries/typo3/standalone-extension-tests.git')
                }
                branch('${CONGO_TRUNK_VERSION_STANDALONE_EXTENSION_TESTS}')
            }
        }
        triggers {
            scm('H 4 * * *')
        }
        steps {
            shell("/usr/local/bin/composer update --no-interaction --no-plugins")
            ant {
                target('prepare behat')
            }

        }
        publishers {
            publishHtml {
                report('') {
                    allowMissing()
                    reportFiles('logs/behat.html')
                    reportName('Behat Test Report')
                }

            }
            junit{
                pattern('logs/behat/*.xml')
            }
        }
    }
}